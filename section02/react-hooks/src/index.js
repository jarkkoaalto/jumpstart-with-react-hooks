
import React, {useState} from "react";
import ReactDOM from "react-dom";


// 2 counter buttons
// 1,5

// Custom hooks
function useCounter({initialState}){
    const[count, setCount] = useState(initialState);
    const increment = () => setCount(count + 1);
    const decrement = () => setCount(count - 1);

    return[count, {
        increment,
        decrement,
        setCount
    }]
}

// Different styles
function App(){

    const[count, {increment, decrement}] = useCounter({initialState:0});

    return (
        <div>
            <p>{count}</p>
            <button onClick={increment}>Inc</button>
            <button onClick={decrement}>Dec</button>
        </div>
    )

}

function App2(){
    const [count,{increment,decrement}] = useCounter({initialState:10});
}

const root = document.getElementById("root");
ReactDOM.render(<App />, root);

/*
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
*/