import React, {useState} from "react";
import ReactDOM from "react-dom";


function Example(){
    // Declare a new state variable, which we'll call count
    const [count, setCount] = useState(0);

    return(
        <div>
            <p>You clicked {count} times</p>
            <button onClick={() => setCount(count + 1)}>
                Hooks in action
            </button>
        </div>
    )
}

const rootElem = document.getElementById("root");
ReactDOM.render(<Example />, rootElem);
// On component update  scroll and listen sockets
// 

/*
class ChatWindow extends React.Component{

    componentDidMount(){
        API.subscribe(this.props.sockedId);
        window.addEventListener('scroll', )
    }
}





import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
*/