import React, {useState, useEffect, useRef, useMemo, useReducer} from "react";
import ReactDOM from "react-dom";

// Fn -> action -> state

function reducer(state, action){

    // DON'T CHANGE STATE
    // return always a new OBJECT
    switch (action.type){
        case "GB":
            return "Περιεχόμενο κεφαλίδας";
        default:
            return state;
    }
}


function useRandomNames(initialValue){
    const [country, dispatch] = useReducer(reducer,'Header Content',{type: 'US'});
    console.log("current state from reducer" , country);

   
    const[inputValue, setInputValue] = useState(initialValue);
    const [randomLength, setRandomLength] = useState(1);
    const[isLoading, setIsLoading] = useState(false);
    const[names, setNames] = useState([]);
    let nameRef = useRef();

    const letterCount = useMemo(()=> makeId((randomLength),[randomLength]));

    const handleInputChange = () => {
        setInputValue(nameRef.current.value)
    };

    useEffect(() => {
        setIsLoading(true);
        fetch(`https://uinames.com/api/?amount=10&region=${inputValue}`)
        .then(response => response.json())
        .then(data => {
            setNames(data);
            setIsLoading(false);
        })
    },[inputValue]);

    return{
        inputValue,
        onChange: handleInputChange,
        names,
        isLoading,
        nameRef,
        country,
        dispatch,
        letterCount
    }
}

// First time this function value is: 1 Random text belong here1
function makeId(n) {

    console.log("make id is called ... ");
    var text="Random text belong here";
    let b=0;
    while(b < 3000000 * n) b++;
    return text+n;
}


const Header = React.memo( props =>{

    // shouldComponentUpdate
    // React.memo
    console.log("Header is rerendering...", props.countryCode)
    return(
        <div>
            {props.countryCode}
            {props.letterCount}
           
        </div>
    )
});

const ThemeContext  = React.createContext();

function App() {
    const value = useContext(ThemeContext);
    let randomNames = useRandomNames('Romania');

    return(
        <div style={value}>
            <Header
                countryCode={randomNames.country}
                letterCount={randomNames.letterCount}
            />
            <p>
                Enter the reqion : {' '}
                <input ref={randomNames.nameRef} />
                <button onClick={randomNames.onChange}>Change</button>
                <button onClick={randomNames.dispatch.bind(this,{type:'GB'})}>
                    Change Country
                </button>
            </p>
            {randomNames.isLoading ? <p>Loading ....</p> :
        <div>
            {
                randomNames.names.length >= 0 && randomNames.names.map((items, i) => (
                <div key={i}>
                    {items.name} {items.gender}
                    </div>
                ))
            }
        </div>}
        }
        </div>
    )

}


const rootElement = document.getElementById("root");
ReactDOM.render(
<ThemeContext.Provider value={{backgroundColor: 'lightblue'}}>
<App />
</ThemeContext.Provider>
, rootElement);